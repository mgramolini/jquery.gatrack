/*
Genuine Interactive
jQuery Google Analytics Tracking Plugin
Author: M.Gramolini
Version: 1.2.1
REMARKS:
All external and document/assets links are tracked in Google Analytics by default.
However, the default GA values easy overridden by adding data-XXXXX attributes.

Ex.
$('.lazy-loaded-slider').gaTrack('click', 'a', { gaeventlabel: 'Slider link' });

The following are equivalent:

1.  $.gaTrack();

    // each object can also have a childselector
2.  var options = [{
            selector: 'a',
            types: 'click'
            }, {
            selector: 'form',
            types: 'submit',
            options: {
            gaeventcategory: 'form-submit'
            }
        }];

    $.gaTrack(options);

3.  $('a').gaTrack('click');
    $('form').gaTrack('submit', { gaeventcategory: 'form-submit' });


Immediately trigger examples:

1. $('#contactUsForm').gaTrack({gaeventaction: 'submit'}); you can override any properties here

2. $('#contactUsForm').gaTrack(); gaeventaction will equal 'trigger' unless overriden on the data attribute

CUSTOMIZATION EXAMPLES:

External links:
(no required data attributes; showing generated default values)
<a href="http://www.facebook.com/" title="Blogger" target="_blank" data-ga="track" data-gaeventcategory="outbound-link" data-gaeventaction="click" data-gaeventlabel="www.facebook.com/" data-gaeventvalue="1" data-gaeventbounce="false">External Link</a>
<a href="//www.facebook.com/" title="Blogger" target="_blank" data-ga="track" data-gaeventcategory="outbound-link" data-gaeventaction="click" data-gaeventlabel="www.facebook.com/" data-gaeventvalue="1" data-gaeventbounce="false">External Link</a>

Internal links:
(data-ga is a required attribute; must not be undefined, null, or empty. Generated defaults are also shown.)
<a href="/helloworld" title="Say hello" data-ga="track" data-gaeventcategory="internal-link" data-gaeventaction="click" data-gaeventlabel="www.example.org/helloworld" data-gaeventvalue="1" data-gaeventbounce="false">Internal Link</a>

Document links:
(no required data attributes; showing generated default values)
<a href="/app_assets/healthnet-plan.pdf" data-gapageview="/documents/app_assets/healthnet-plan.pdf">Document Link</a>

Hash links:
(no required data attributes; showing generated default values)
<a href="#blog" title="Blogger" data-ga="track" data-gaeventcategory="hash-link" data-gaeventaction="click" data-gaeventlabel="blog" data-gaeventvalue="1" data-gaeventbounce="false">Hash Link</a>

Ux control links:
(no required data attributes; showing generated default values)
<a href="#" id="btnSliderRight" title="Next slide" data-ga="track" data-gaeventcategory="ux-control-link" data-gaeventaction="click" data-gaeventlabel="btnSliderRight" data-gaeventvalue="1" data-gaeventbounce="false">UX Control Link</a>

Form submissions:
(data-ga is a required attribute; must not be undefined, null, or empty. Generated defaults are also shown.)
<form id="myform" action="/vapi/form.json" data-ga="track" data-gaeventcategory="form-submit" data-gaeventaction="submit" data-gaeventlabel="www.example.org/vapi/form.json" data-gaeventvalue="1" data-gaeventbounce="false">
<input id="submit" name="submit" type="submit" value="submit" />
</form>
*/




// TODO: probably don't need bounce - check docs

(function ($) {
    var execFunc = function () { }; // fallback so we don't throw errors when console doesn't exist
     var transform = function(args) {
        if(window.ga){
            args[0] = args[0].replace('_track', '').toLowerCase();
            args.unshift('send');
        }
    };


    var setupMultipleTrackers = function () {
        var trackers;
        if(window._gat){
            trackers = window._gat._getTrackers();
        }
        else{
            trackers = window.ga.getAll();
        }

        if (trackers.length > 1) {
            execFunc = function (args) {
                transform(args);
                var fn = args.splice(0, 1);
                $.each(trackers, function () {
                    this[fn].apply(this, args);
                });
            };
        }
    };

     if (!(window._gaq || window.ga)) {
        if (window.console && console.log) {
            console.warn("Google tracking isn't initialized, setting execFunc to console.log");
            execFunc = function (args) {
                transform(args);
                console.log('gaTrack: ', args);
            };
        }
    } else if (typeof window._gaq !== 'undefined') {
        execFunc = function (args) {
            window._gaq.push(args);
        };

        window._gaq.push(setupMultipleTrackers);
    } else if (typeof window.ga !== 'undefined'){
        execFunc = function(args){
            transform(args);
            window.ga.apply(window.ga, args);
        }

        window.ga(setupMultipleTrackers);
    }

    $.fn.extend({
        gaTrack: function (types, selector, options) {

            //---- Define default values ----
            var defaults = {
                gaeventcategory: 'outbound-link',
                gaeventvalue: 1,
                gaeventbounce: false /* A Boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation. */
            };

            var trackPageViewExtensions = ['pdf', 'doc', 'docx'];

            // TODO: Carlos/Matt -- check order of overriding values
            function track(event) {
                // Override defaults
                var opts = $.extend({ gaeventaction: event.type }, defaults, options);
                // Prevent multiple calls to jQuery constructor
                var $this = $(this);
                // Determine reference location (should we check 'src'?)
                var href = $this.attr('href') || $this.attr('action') || '';

                // Check if href starts with 'http' or '//'
                if (href.lastIndexOf('http', 0) === 0 || href.lastIndexOf('//', 0) === 0) {
                    // Set event label to host/pathandquery
                    opts.gaeventlabel = href.substring(href.lastIndexOf('//', 8) + 2);
                    // Track this event
                    opts.ga = 'track';
                } // Check if href starts with '#'
                else if (href.lastIndexOf('#', 0) === 0) {
                    // Set event label to whatever comes after '#'
                    var label = href.substring(1);
                    // Track this event
                    opts.ga = 'track';
                    if (label) {
                        // If there is an event label, it is a hash link
                        opts.gaeventcategory = 'hash-link';
                        opts.gaeventlabel = label;
                    }
                    else {
                        // No event label, must be ux control link
                        opts.gaeventcategory = 'ux-control-link';
                    }
                }
                else if (href.lastIndexOf('mailto:', 0) === 0) {

                    // Set event label to the address
                    opts.gaeventlabel = href.substring(7);

                    // Track this event
                    opts.ga = 'track';

                    // Category is mailto-link
                    opts.gaeventcategory = 'mailto-link';
                } // Internal link or form submit
                else {
                    // Set event label to the host/pathandquery or reference location
                    if (!opts.gaeventlabel) {
                        opts.gaeventlabel = this.href ? this.href.substring(this.href.lastIndexOf('//', 8) + 2) : href;
                    }

                    // Use overridden value (case of form) or internal link
                    if (!opts.gaeventcategory) {
                        opts.gaeventcategory = (options || {}).gaeventcategory || 'internal-link';
                    }
                }

                // Override the options


                // TODO: trackevent not just event
                var overrides = $.extend({ gamethod: '_trackEvent', gaeventlabel: this.id || 'no label' }, opts, $this.data());

                if ($.inArray(href.split('.').pop(), trackPageViewExtensions) !== -1 || (overrides.ga && overrides.gamethod === '_trackPageview')) {
                    //GA _trackPageview code here using trackUrl
                    execFunc([
                        '_trackPageview',
                        overrides.gapageview || '/documents' + href
                    ]);
                } else if (overrides.ga) {
                    //GA _trackEvent code below using options object
                    execFunc([
                        overrides.gamethod,
                        overrides.gaeventcategory,
                        overrides.gaeventaction,
                        overrides.gaeventlabel,
                        overrides.gaeventvalue,
                        overrides.gaeventbounce
                    ]);
                }
            }

            if (!types || typeof types === 'object') {
                // explicitly being called on jquery object
                // types can be undefined or an object with override options
                options = $.extend({ ga: 'track' }, types);
                track.call(this, { type: 'trigger' });

                return this;
            } else if (!options) {
                if (typeof selector !== "string") {
                    // ( types, options )
                    options = selector;
                    selector = undefined;
                }
                // otherwise
                // ( types, selector )
            }

            return this.on(types, selector, track);
        }
    });
    $.extend({
        gaTrack: function (options) {
            var defaults = [{
                selector: 'a',
                types: 'click'
            }];

            options = options || defaults;

            $.each(options, function (index, value) {
                $(value.selector).gaTrack(value.types, value.childselector, value.options);
            });
        }
    });
}(jQuery));
